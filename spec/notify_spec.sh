#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'notify'
    startup() {
        echo "@test please review" > slack_message.txt
    }

    cleanup() {
        rm -f slack_message.txt
    }
    Before 'startup'
    After 'cleanup'

    Mock send_slack_notification
    End

    It 'can notify'
        When run script scripts/notify.sh
        The status should be success
        The stdout should include "@test please review"
        The stderr should include "send_slack_notification --message-file slack_message.txt"
    End
End
