#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

submit2dw() {
    if [[ "${DRY_RUN:-}" == "1" ]]; then
        return
    fi
    kcidb_tool push2umb -i output.json --certificate /tmp/umb_certificate.pem
    # Try to upload the results synchronously.
    # Failures are ignored as another round of uploading is handled external to
    # the pipeline via datawarehouse-submitter.
    if ! python3 -m cki_tools.datawarehouse_submitter --kcidb-file ./output.json; then
        echo "FAIL: couldn't submit results to DW. Let's wait for DW synchronous job to run"
        sleep 10m
    fi
}
