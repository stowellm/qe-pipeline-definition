#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

set -x

cat << EOF > slack_message.txt
Error on CI pipeline (${TEST_JOB_NAME:-}): ${CI_SERVER_URL:-}/${CI_PROJECT_NAMESPACE:-}/${CI_PROJECT_NAME:-}/pipelines/${CI_PIPELINE_ID:-}

@bgoncalv please review...
EOF

cat slack_message.txt

if [[ "${DRY_RUN:-}" != "1" ]]; then
    send_slack_notification --message-file slack_message.txt
fi

if [[ "${REPORT_OSCI:-}" == "1" ]]; then
    OSCI_TEST_STATUS=error scripts/osci_report_results.sh
fi
